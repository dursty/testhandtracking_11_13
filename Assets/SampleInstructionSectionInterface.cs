﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleInstructionSectionInterface : InstructionSectionInterface {

	private TextMesh txt;

	void Awake(){
		txt = GetComponent<TextMesh>();
	}

	public override void onNewSection(InstructionSection section){
		InstructionText wrapper = new InstructionText();
		float cWidth = wrapper.getCharacterWidth(txt);
		float pWidth = wrapper.getPanelWidth(txt.transform.parent);
		int maxChar = (int)(pWidth/cWidth);

		txt.text = wrapper.fixOverflow("Section Name: "+ section.getName(),maxChar*2);
	}
}
