﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clickable2 : MonoBehaviour{

	public InstructionDatabase database;

	void OnMouseDown() {
        database.prevInstruction();
    }
}
