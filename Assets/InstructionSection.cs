﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

public class InstructionSection {
	[XmlAttribute("id")]
	public int id;

    [XmlAttribute("name")]
    public string name;

    [XmlArray("Instructions")]
 	[XmlArrayItem("Instruction")]
    public List<InstructionStep> instructions;

    public List<InstructionStep> getInstructions(){
        return instructions;
    }

    public InstructionStep getInstruction(int i){
        return instructions[i];
    }

	public int getID(){
		return id;
	}

    public string getName(){
        return name;
    }
}
