﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionDatabase : MonoBehaviour {

	public List<InstructionReceiverInterface> receivers;
	public Transform arrowPrefab;
	public InstructionSectionInterface sectionText;
	public TextAsset xmlFile;

	private InstructionSet instructionSet;
	static List<InstructionStep> currentInstructions;
	static int currentSection = 0;
	static int currentInstruction = 0;

	// Use this for initialization
	void Awake(){
		instructionSet = InstructionSet.loadFromFile(xmlFile.text);
		currentInstructions = instructionSet.getInstructions(currentSection);
	}

	void Start () {
		notifyReceivers(currentInstructions[0]); // Send first instruction to everyone
		sectionText.onNewSection(instructionSet.getSection(currentSection));
	}

	public InstructionStep nextInstruction(){
		if(currentSection == instructionSet.getSectionCount()-1 && currentInstruction == currentInstructions.Count - 1){
			return currentInstructions[currentInstructions.Count -1];
		}

		// End of this section
		if(currentInstruction >= currentInstructions.Count - 1){
			currentSection++;
			currentInstruction = -1; // Because this gets ++1 after

			if(currentSection < instructionSet.getSectionCount()){
				currentInstructions = instructionSet.getInstructions(currentSection);
				sectionText.onNewSection(instructionSet.getSection(currentSection));
			}else{
				// return last instruction if overflow, should handle this better
				return currentInstructions[currentInstructions.Count -1];
			}
		}

		currentInstruction++;
		InstructionStep instruction = currentInstructions[currentInstruction];
		notifyReceivers(instruction);
		return instruction;
	}

	public InstructionStep prevInstruction(){
		if(currentInstruction == 0 && currentSection == 0){
			 // first instruction in set, can't go further back
			 return currentInstructions[0];
		}else if(currentInstruction == 0){
			currentSection--;
			currentInstructions = instructionSet.getInstructions(currentSection);
			currentInstruction = currentInstructions.Count - 1;
			sectionText.onNewSection(instructionSet.getSection(currentSection));
		}else{
			currentInstruction--;
		}

		Debug.Log(currentInstruction);
		InstructionStep instruction = currentInstructions[currentInstruction];
		notifyReceivers(instruction);
		return instruction;
	}

	public void notifyReceivers(InstructionStep instruction){
		for(int i = 0;i<receivers.Count;i++){
			receivers[i].onNewInstruction(instruction);
		}
	}

	public Transform getPrefab(DisplayObject.ModelType modelType){
		if(modelType == DisplayObject.ModelType.Arrow){
			return arrowPrefab;
		}else{
			return null;
		}
	}
}
