﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Windows.Speech;

public class FollowHandUI : MonoBehaviour
{
	/*
	This is the class that is attached to the UI gameObject which handles hand tracking as well as voice recognition 
	which is related to the UI.
	*/

	// the instruction database object which will be used to access the next instruction
    public InstructionDatabase database;
	
	// the indicator gameobject is used to instantiate the UI object and then later used to show/hide it
    private GameObject indicator = null;
	
	// the text mesh object used to display the debug text containing the position and speed info 
    private TextMesh textMesh = null;
	
	// this is used to determine whether the object has to be stopped in place or not
    private bool flagToStopObject = false;
	
	// the UI game object
    public GameObject UI;
	
	// this is the manager object of the UI game object 
    public GameObject ManagerUI;
	
	// this is the game object which contains the instructions to be displayed
    public GameObject instructionText;
	
	// this is the section text object used to display the section text
    public GameObject sectionText;
	
	// this is the counter to determine what instruction number the current instruction is 
    public int instructionCounter = 0;


    // SpeechManager code START

	// the below two line will start the keyword recognition code
    KeywordRecognizer keywordRecognizer = null;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

	// this function will get the value of the keyword and invoke the action associated to it
    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }


    // SpeechManager code END

	// this function will create the object at the particular location 
    private void CreateIndicator()
    {
        if (indicator == null)
        {

            indicator = ManagerUI;
            indicator.transform.localScale = new Vector3(0.04f, 0.04f, 0.04f);
        }
    }

	// this function will update the location of the game object based on the position passed as the parameter
    private void UpdateIndicator(Vector3 position)
    {
        if (indicator != null)
        {

            indicator.transform.position = position;
        }
    }

	// this function will create the text mesh game object
    private void CreateText()
    {
        GameObject text = new GameObject();
        textMesh = text.AddComponent<TextMesh>();
        //text.AddComponent<GameObject>();
        text.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
    }
	
	// this function will update the text mesh object to a location above the location passed as the parameter
    private void UpdateText(Vector3 position, Vector3 velocity)
    {
        if (textMesh != null)
        {
            position = new Vector3(position.x, position.y + 0.1f, position.z);
            textMesh.gameObject.transform.position = position;
            var gazeDirection = Camera.main.transform.forward;
            textMesh.gameObject.transform.rotation = Quaternion.LookRotation(gazeDirection);
            textMesh.text = string.Format("Position:{0:0.00},{1:0.00},{2:0.00}\n Velocity: {3:0.00},{4:0.00},{5:0.00}", position.x, position.y, position.z, velocity.x, velocity.y, velocity.z);
        }
    }

	// this will set flags of the respective objects to make them show up
    public void ShowObjects(bool show)
    {
        if (indicator != null && textMesh != null)
        {
            indicator.SetActive(show);
            textMesh.gameObject.SetActive(show);
        }
    }

    void Start()
    {
		// setting the initial instrction counter
        instructionCounter = 0;

        // CODE FOR SPEECH RECOGNITION START
		// the below blocks of code will add separate keywords or phrases which need to be recognized 
        keywords.Add("Next", () => {
                database.nextInstruction();
            instructionCounter += 1;
        });

        keywords.Add("Previous", () => {
            database.prevInstruction();
            instructionCounter -= 1;
        });

        keywords.Add("Reset", () => {
            for (; instructionCounter > 0; instructionCounter--)
            {
                database.prevInstruction();
            }
        });


        keywords.Add("Stop UI", () =>
        {
            flagToStopObject = true;
        });



        keywords.Add("Start UI", () =>
		{
        flagToStopObject = false;

		});

		// below block of code will hide the UI game object by setting some flags 
        keywords.Add("Hide UI", () =>
        {
            flagToStopObject = true;
            UI.GetComponent<Renderer>().enabled = false;
            instructionText.GetComponent<Renderer>().enabled = false;
            sectionText.GetComponent<Renderer>().enabled = false;
        });

		// below block of code will show the UI game object by setting some flags
        keywords.Add("Show UI", () =>
        {
            flagToStopObject = true;
            UI.GetComponent<Renderer>().enabled = true;
            instructionText.GetComponent<Renderer>().enabled = true;
            sectionText.GetComponent<Renderer>().enabled = true;
        });


        // Tell the KeywordRecognizer about our keywords.
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());

        // Register a callback for the KeywordRecognizer and start recognizing!
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
        // CODE FOR SPEECH RECOGNITION END

		// calling the functions required to be called on start 
        CreateIndicator();
        CreateText();
        alwaysStayOriented();
		
		// getting the different kinds of inputs and assiging callbacks to them
        UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceLostLegacy += InteractionManager_SourceLost;
        UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceDetectedLegacy += InteractionManager_SourceDetected;
        UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceUpdatedLegacy += InteractionManager_SourceUpdated;
    }

	// this function will be called whenever the source (hand) is changed. we then set the objects to be shown 
    private void InteractionManager_SourceDetected(UnityEngine.XR.WSA.Input.InteractionSourceState state)
    {
        
        if (state.source.kind == UnityEngine.XR.WSA.Input.InteractionSourceKind.Hand)
        {
            ShowObjects(true);

        }
    }

	// this function will be called whenever the source (hand) is lost. we then set the objects to be hidden 
    private void InteractionManager_SourceLost(UnityEngine.XR.WSA.Input.InteractionSourceState state)
    {
        
        if (state.source.kind == UnityEngine.XR.WSA.Input.InteractionSourceKind.Hand && flagToStopObject == false)
        {
            ShowObjects(false);

        }
    }

	// this function will be called whenever the source (hand) is updated. we then set the positions of the objects and then update them 
    private void InteractionManager_SourceUpdated(UnityEngine.XR.WSA.Input.InteractionSourceState state)
    {
        
        if (state.source.kind == UnityEngine.XR.WSA.Input.InteractionSourceKind.Hand)
        {
            Vector3 handPosition;
            Vector3 handVelocity;

            state.sourcePose.TryGetPosition(out handPosition);
            state.sourcePose.TryGetVelocity(out handVelocity);


            //handPosition.z = handPosition.z + 0.8f;
            if (flagToStopObject == false)
            {
                // uncomment below line of code to show debug text with coordinates and speed
                //UpdateText(handPosition, handVelocity);
                UpdateIndicator(handPosition);
            }


        }
    }

	// this will be called every frame 
    void Update()
    {
        alwaysStayOriented();
    }


	// the below function will be called every frame (since its called from Update() to orient the respective objects so that they always 
	// face the user. 
    private void alwaysStayOriented()
    {
        var gazeDirection = Camera.main.transform.forward;
        gazeDirection.y = 0.0f;
        indicator.transform.rotation = Quaternion.LookRotation(gazeDirection);
        indicator.transform.Rotate(Vector3.right * (-90));
    }


}
