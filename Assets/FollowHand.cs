﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Windows.Speech;

public class FollowHand : InstructionReceiverInterface
{
	/*
	This class is attached to the main model game object of the project and contains all the necessary
	position update and keyword recognition as well
	*/

	// the game object indicator will be the main game object associated to changing the position etc
    private GameObject indicator = null;
	
	// the game object text mesh will display some debug text related to the position and speed of the object
    private TextMesh textMesh = null;
	
	// this is the flag which determine whether the object needs to be stopped or not
    private bool flagToStopObject = false;
	
	// thi is the flag which determines whether model roatation is activated or not 
    private bool flagToStartModelRotation = false;
	
	// this is the manager game object of the main model 
    public GameObject Manager;
	
	// this is the outer model game object of the project 
    public GameObject OuterModel;
	
	// this is the inner model game object of the project
    public GameObject InnerModel;
	
	// this will indicate which game object (inner or outer) is the current game object
    public string CurrentModel;
	
	// these are the arrow game objects which will be updated according to the location in the XML file 
    public GameObject Arrow1;
    public GameObject Arrow2;
    public GameObject Arrow3;
    public GameObject Arrow4;
    public GameObject Arrow5;
	
	// this is the arrow scale vector used to rescale the arrow to proper size
    public Vector3 ArrowScale;
	
	// this is the default arrow position where the arrow position of the first arrow (arrow1) is taken 
	// at the start of the program and then all arrows are displaced relative to that position 
    public Vector3 DefaultArrowPosition;
	
	// this is the default arrow rotation 
    public Quaternion DefaultArrowRotation;
	
	// this is the default model rotation
    public Quaternion DefaultModelRotation;
	
	// this is the current position of the object
    public Vector3 currentPosition = new Vector3(0, 0, 0);
	
	// this is the flag which will determine whether to hide the model or not
    public bool flagToHideModel = false;
	
	// this is the hand position and velocity to be used later 
    Vector3 handPosition;
    Vector3 handVelocity;

    // SpeechManager code START
    KeywordRecognizer keywordRecognizer = null;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();


	// this function will get the value of the keyword and invoke the action associated to it
    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }


    // SpeechManager code END

	// this function will create the object at the particular location 
    private void CreateIndicator()
    {
        if (indicator == null)
        {
            indicator = Manager;
            indicator.transform.localScale = new Vector3(0.04f, 0.04f, 0.04f);
        }
    }

	// this function will update the location of the game object based on the position passed as the parameter
    private void UpdateIndicator(Vector3 position)
    {
        if (indicator != null)
        {
            indicator.transform.position = position;
            var gazeDirection = Camera.main.transform.forward;
            gazeDirection.y = 0.0f;
            indicator.transform.rotation = Quaternion.LookRotation(gazeDirection);
            currentPosition = new Vector3(position.x, position.y + 0.1f, position.z);
        }
    }

	// this function will create the text mesh game object
    private void CreateText()
    {
        GameObject text = new GameObject();
        textMesh = text.AddComponent<TextMesh>();
        //text.AddComponent<GameObject>();
        text.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
    }

	// this function will update the text mesh object to a location above the location passed as the parameter
    private void UpdateText(Vector3 position, Vector3 velocity)
    {
        if (textMesh != null)
        {
            position = new Vector3(position.x, position.y + 0.1f, position.z);
            textMesh.gameObject.transform.position = position;
            var gazeDirection = Camera.main.transform.forward;
            textMesh.gameObject.transform.rotation = Quaternion.LookRotation(gazeDirection);
            textMesh.text = string.Format("Position:{0:0.00},{1:0.00},{2:0.00}\n Velocity: {3:0.00},{4:0.00},{5:0.00}", position.x, position.y, position.z, velocity.x, velocity.y, velocity.z);
        }
    }

	// this will set flags of the respective objects to make them show up
    public void ShowObjects(bool show)
    {
        if (indicator != null && textMesh != null)
        {
            indicator.SetActive(show);
            textMesh.gameObject.SetActive(show);
        }
    }

    void Start()
    {

        // CODE FOR SPEECH RECOGNITION START

        // set the DefaultArrowPosition to the one at the beginning of the app start
        flagToHideModel = false;
        DefaultArrowPosition = Arrow1.transform.localPosition;
        DefaultArrowRotation = Arrow1.transform.localRotation;
        DefaultModelRotation = OuterModel.transform.localRotation;
        CurrentModel = "outermodel";

		
        //Debug.Log(Arrow1.transform.localScale);

        // code to hide inner model and arrows upon application start
        Arrow1.GetComponent<Renderer>().enabled = false;
        Arrow2.GetComponent<Renderer>().enabled = false;
        Arrow3.GetComponent<Renderer>().enabled = false;
        Arrow4.GetComponent<Renderer>().enabled = false;
        Arrow5.GetComponent<Renderer>().enabled = false;
        InnerModel.GetComponent<Renderer>().enabled = false;

        
        // Color all arrows green
        Arrow1.GetComponent<Renderer>().material.color = Color.green;
        Arrow2.GetComponent<Renderer>().material.color = Color.green;
        Arrow3.GetComponent<Renderer>().material.color = Color.green;
        Arrow4.GetComponent<Renderer>().material.color = Color.green;
        Arrow5.GetComponent<Renderer>().material.color = Color.green;



		// below block of code contains all keywords required in this script 
        keywords.Add("Hide Arrow", hideArrow1);

        keywords.Add("Show Arrows", showArrows);

        keywords.Add("Move Arrow", moveArrow1To);

        keywords.Add("Rotate Arrow", rotateArrow1);

        keywords.Add("Start Model Rotation", () => { flagToStartModelRotation = true; });

        keywords.Add("Stop Model Rotation", () => { flagToStartModelRotation = false; });


        // code for keywords
        keywords.Add("Start Model", () =>
        {
            flagToStopObject = false;

        });

        keywords.Add("Stop Model", () =>
        {
            flagToStopObject = true;

        });


        keywords.Add("Hide Model", () =>
        {
            flagToStopObject = true;
            flagToHideModel = true;
            if (CurrentModel == "outermodel")
                OuterModel.GetComponent<Renderer>().enabled = false;
            else 
                InnerModel.GetComponent<Renderer>().enabled = false;
            //UI.SetActive(false);

        });

        keywords.Add("Show Model", () =>
        {
            flagToStopObject = true;
            flagToHideModel = false;
            if (CurrentModel == "outermodel")
                OuterModel.GetComponent<Renderer>().enabled = true;
            else
                InnerModel.GetComponent<Renderer>().enabled = true;
            //UI.SetActive(true);

        });

        keywords.Add("Translate Model", () =>
        {
            //flagToTransform = true;
            indicator.transform.Translate(0, 0, 0.5f);
            //UI.SetActive(true);


        });




        // Tell the KeywordRecognizer about our keywords.
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());

        // Register a callback for the KeywordRecognizer and start recognizing!
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
        // CODE FOR SPEECH RECOGNITION END

        CreateIndicator();
        CreateText();
        UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceLostLegacy += InteractionManager_SourceLost;
        UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceDetectedLegacy += InteractionManager_SourceDetected;
        UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceUpdatedLegacy += InteractionManager_SourceUpdated;


    }

	// this function will be called whenever the source (hand) is changed. we then set the objects to be shown 
    private void InteractionManager_SourceDetected(UnityEngine.XR.WSA.Input.InteractionSourceState state)
    {
        if (state.source.kind == UnityEngine.XR.WSA.Input.InteractionSourceKind.Hand)
        {
            ShowObjects(true);
        }
    }

	// this function will be called whenever the source (hand) is lost. we then set the objects to be hidden 
    private void InteractionManager_SourceLost(UnityEngine.XR.WSA.Input.InteractionSourceState state)
    {
        if (state.source.kind == UnityEngine.XR.WSA.Input.InteractionSourceKind.Hand && flagToStopObject == false)
        {
            ShowObjects(false);
        }
    }

	// this function will be called whenever the source (hand) is updated. we then set the positions of the objects and then update them 
    private void InteractionManager_SourceUpdated(UnityEngine.XR.WSA.Input.InteractionSourceState state)
    {
        if (state.source.kind == UnityEngine.XR.WSA.Input.InteractionSourceKind.Hand)
        {

            state.sourcePose.TryGetPosition(out handPosition);
            state.sourcePose.TryGetVelocity(out handVelocity);

            if (flagToStopObject == false)
            {

                // uncomment below line of code to show debug text showing coordinates and speed 
                //UpdateText(handPosition, handVelocity);
                UpdateIndicator(handPosition);
            }

            if (flagToStartModelRotation == true)
            {
                startModelRotation();
            }


        }
    }




	// this function will be called whenever a new instruction needs to be fetched
    public override void onNewInstruction(InstructionStep instruction) {

        // This function is not getting called

        Debug.Log("Inside the onNewInstruction function");

        hideAllArrows();

		// determining what is the current model and setting indicator to that object 
        if ("innermodel" == instruction.mainModel)
        {
            CurrentModel = "innermodel";
            if (!flagToHideModel)
            {
                OuterModel.GetComponent<Renderer>().enabled = false;
                InnerModel.GetComponent<Renderer>().enabled = true;
            }
        }

        else if ("outermodel" == instruction.mainModel)
        {
            CurrentModel = "outermodel";
            if (!flagToHideModel)
            {
                OuterModel.GetComponent<Renderer>().enabled = true;
                InnerModel.GetComponent<Renderer>().enabled = false;
            }
        }
        else
        {
            CurrentModel = "outermodel";
            OuterModel.GetComponent<Renderer>().enabled = false;
            InnerModel.GetComponent<Renderer>().enabled = false;
        }
        

		// for the current instrucion, get the number of display objects and iterate that many times 
		// then we show the arrow with the counter number and move to the position and rotate according 
		// to the data in the XML file 
        List<DisplayObject> objects = instruction.getDisplayObjects();
        for(int i = 0; i < objects.Count; i++)
        {

            showArrow(i);
            moveArrow(i, objects[i].getTranslateCoord());
            rotateArrow(i, objects[i].getOrientation());


        }
    }


	// this is the function which will move the arrows. we determine which arrow to move by passing the 'i' 
	// argument which will indicate the arrow number and then the coordinate where to move. note that 
	// all the movement is relative to the position of Arrow1 when the program starts. 
    private void moveArrow(int i,Vector3 coord)
    {
        switch (i)
        {
            case 0:
                Arrow1.transform.localPosition = DefaultArrowPosition;
                Arrow1.transform.localRotation = DefaultArrowRotation;
                Arrow1.transform.Translate(coord);
                break;
            case 1:
                Arrow2.transform.localPosition = DefaultArrowPosition;
                Arrow2.transform.localRotation = DefaultArrowRotation;
                Arrow2.transform.Translate(coord);
                break;
            case 2:
                Arrow3.transform.localPosition = DefaultArrowPosition;
                Arrow3.transform.localRotation = DefaultArrowRotation;     
                Arrow3.transform.Translate(coord);
                break;
            case 3:
                Arrow4.transform.localPosition = DefaultArrowPosition;
                Arrow4.transform.localRotation = DefaultArrowRotation;
                Arrow4.transform.Translate(coord);
                break;
            case 4:
                Arrow5.transform.localPosition = DefaultArrowPosition;
                Arrow5.transform.localRotation = DefaultArrowRotation;
                Arrow5.transform.Translate(coord);
                break;

            default:

                break;
        }
    }


	// this function will rotate the arrow according to the direction which will be received 
	// from the instruction. 'i' is the arrow number which needs to be rotated and direction will 
	// indicate which direction the arrow will be pointing at 
    private void rotateArrow(int i, string direction)
    {
        GameObject currentArrow = new GameObject();

		// setting the current arrow for changing it further 
        switch (i)
        {
            case 0:
                currentArrow = Arrow1;
                break;
            case 1:
                currentArrow = Arrow2;
                break;
            case 2:
                currentArrow = Arrow3;
                break;
            case 3:
                currentArrow = Arrow4;
                break;
            case 4:
                currentArrow = Arrow5;
                break;

            default:
                break;
        }


		// switching according to the direction 
        switch (direction)
        {
            case "down":
                currentArrow.transform.localRotation = DefaultArrowRotation;
				
				// the below line of code will stop all BobbingAnimation coroutines if any 
                currentArrow.GetComponent<BobbingAnimation>().StopAllCoroutines();
				
				// this will stop the BobbingAnimation of the arrow so that it syncs with others. this will be done at the very beginning of the start of the coroutine
                currentArrow.GetComponent<BobbingAnimation>().floatFlag = false;
				
				// then we set the direction of the current arrow object 
                currentArrow.GetComponent<BobbingAnimation>().objectDirection = "down";
                break;
            case "right":
                currentArrow.transform.localRotation = DefaultArrowRotation;
				
				// this will rotate the arrow according to the direction
                currentArrow.transform.rotation = Quaternion.LookRotation(indicator.transform.forward) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
				
				// the below line of code will stop all BobbingAnimation coroutines if any 
                currentArrow.GetComponent<BobbingAnimation>().StopAllCoroutines();
				
				// this will stop the BobbingAnimation of the arrow so that it syncs with others. this will be done at the very beginning of the start of the coroutine
                currentArrow.GetComponent<BobbingAnimation>().floatFlag = false;
				
				// then we set the direction of the current arrow object 
                currentArrow.GetComponent<BobbingAnimation>().objectDirection = "right";
                break;
            case "up":
                currentArrow.transform.localRotation = DefaultArrowRotation;
				
				// this will rotate the arrow according to the direction
                currentArrow.transform.rotation = Quaternion.LookRotation(indicator.transform.forward) * Quaternion.AngleAxis(180, new Vector3(1, 0, 0));
				
				// the below line of code will stop all BobbingAnimation coroutines if any 
                currentArrow.GetComponent<BobbingAnimation>().StopAllCoroutines();
				
				// this will stop the BobbingAnimation of the arrow so that it syncs with others. this will be done at the very beginning of the start of the coroutine
                currentArrow.GetComponent<BobbingAnimation>().floatFlag = false;
				
				// then we set the direction of the current arrow object 
                currentArrow.GetComponent<BobbingAnimation>().objectDirection = "up";
                break;
            case "left":
                currentArrow.transform.localRotation = DefaultArrowRotation;
				
				// this will rotate the arrow according to the direction
                currentArrow.transform.rotation = Quaternion.LookRotation(indicator.transform.forward) * Quaternion.AngleAxis(270, new Vector3(0, 0, 1));
				
				// the below line of code will stop all BobbingAnimation coroutines if any 
                currentArrow.GetComponent<BobbingAnimation>().StopAllCoroutines();
				
				// this will stop the BobbingAnimation of the arrow so that it syncs with others. this will be done at the very beginning of the start of the coroutine
                currentArrow.GetComponent<BobbingAnimation>().floatFlag = false;
				
				// then we set the direction of the current arrow object 
                currentArrow.GetComponent<BobbingAnimation>().objectDirection = "left";
                break;
            case "forward":
                currentArrow.transform.localRotation = DefaultArrowRotation;
				
				// this will rotate the arrow according to the direction
                currentArrow.transform.rotation = Quaternion.LookRotation(indicator.transform.forward) * Quaternion.AngleAxis(-90, new Vector3(1, 0, 0));
				
				// the below line of code will stop all BobbingAnimation coroutines if any 
                currentArrow.GetComponent<BobbingAnimation>().StopAllCoroutines();
				
				// this will stop the BobbingAnimation of the arrow so that it syncs with others. this will be done at the very beginning of the start of the coroutine
                currentArrow.GetComponent<BobbingAnimation>().floatFlag = false;
				
				// then we set the direction of the current arrow object 
                currentArrow.GetComponent<BobbingAnimation>().objectDirection = "forward";
                break;
            case "back":
                currentArrow.transform.localRotation = DefaultArrowRotation;
				
				// this will rotate the arrow according to the direction
                currentArrow.transform.rotation = Quaternion.LookRotation(indicator.transform.forward) * Quaternion.AngleAxis(90, new Vector3(1, 0, 0));
				
				// the below line of code will stop all BobbingAnimation coroutines if any 
                currentArrow.GetComponent<BobbingAnimation>().StopAllCoroutines();
				
				// this will stop the BobbingAnimation of the arrow so that it syncs with others 
                currentArrow.GetComponent<BobbingAnimation>().floatFlag = false;
				
				// then we set the direction of the current arrow object 
                currentArrow.GetComponent<BobbingAnimation>().objectDirection = "back";
                break;
            default:
                currentArrow.transform.localRotation = DefaultArrowRotation;
                break;
        }


    }


	// this function will hide all the arrows 
    private void hideAllArrows()
    {

        Arrow1.GetComponent<Renderer>().enabled = false;
        Arrow2.GetComponent<Renderer>().enabled = false;
        Arrow3.GetComponent<Renderer>().enabled = false;
        Arrow4.GetComponent<Renderer>().enabled = false;
        Arrow5.GetComponent<Renderer>().enabled = false;
    }


	// this function will show the arrow which will be indicated by 'i', the arrow number 
    private void showArrow(int i)
    {
        switch (i)
        {
            case 0:
                Arrow1.GetComponent<Renderer>().enabled = true;
                break;
            case 1:
                Arrow2.GetComponent<Renderer>().enabled = true;
                break;
            case 2:
                Arrow3.GetComponent<Renderer>().enabled = true;
                break;
            case 3:
                Arrow4.GetComponent<Renderer>().enabled = true;
                break;
            case 4:
                Arrow5.GetComponent<Renderer>().enabled = true;
                break;

            default:
                break;
        }
    }


    // code to move arrow 1
    private void moveArrow1To()

    {
        Arrow1.transform.Translate(-1.3f, 0, 0);
    }


    // code to rotate arrow 1
    private void rotateArrow1()

    {
        Arrow1.transform.rotation = Quaternion.AngleAxis(30, Vector3.right);
    }

    // code to show all arrows 
    private void showArrows()

    {
        Arrow1.GetComponent<Renderer>().enabled = true;
        Arrow2.GetComponent<Renderer>().enabled = true;
        Arrow3.GetComponent<Renderer>().enabled = true;
        Arrow4.GetComponent<Renderer>().enabled = true;
        Arrow5.GetComponent<Renderer>().enabled = true;
        Debug.Log("Working");
    }

    // code to hide arrow 1
    private void hideArrow1()

    {
        Arrow1.GetComponent<Renderer>().enabled = false;
    }


	// this function will start the rotation of the farther end of the model and will track the finger/hand 
    private void startModelRotation()
    {
        Vector3 tempVector = indicator.transform.position - handPosition;
        tempVector.y = 0.0f;
        indicator.transform.rotation = Quaternion.LookRotation(tempVector) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0));
    }


}
