﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BobbingAnimation : MonoBehaviour {
	/*
	This class needs to be attached to any arrow object which will need to be animated. The way this class has been coded 
	is that after setting the direction of the arrow, the animation will be running continuously and in order to change 
	the direction of the bobbing, we have to first set the floatFlag to false and then set the objectDirection flag to 
	the direction we got from the current instruction. 
	*/

	// this needs to be false when starting the animation anew or when change of direction occurs
    public bool floatFlag;
	
	// this will change the bobbing direction of the object
    public string objectDirection;
	
	// this is the distance the arrow will bob for 
    public float distanceToFloat = 0.2f;
	
	// this will indicate how much time the animation will run for 
    public int WaitForSeconds = 1;

	// Use this for initialization
	void Start () {
        floatFlag = false;
		
	}
	
	// Update is called once per frame
	void Update () {
        
        switch (objectDirection)
        {
            case "down":
                startFloatingUp();
                break;
            case "left":
                startFloatingRight();
                break;
            case "right":
                startFloatingLeft();
                break;
            case "up":
                startFloatingDown();
                break;
            case "forward":
                startFloatingBack();
                break;
            case "back":
                startFloatingForward();
                break;
        }
    }

    // below code for starting and stopping the parts of the floating
    public void startFloatingUp()
    {

        if (floatFlag)
            StartCoroutine(floatingDownF());
        else if (!floatFlag)
            StartCoroutine(floatingUpT());

    }

    public void startFloatingDown()
    {

        if (floatFlag)
            StartCoroutine(floatingUpF());
        else if (!floatFlag)
            StartCoroutine(floatingDownT());

    }

    public void startFloatingRight()
    {

        if (floatFlag)
            StartCoroutine(floatingLeftF());
        else if (!floatFlag)
            StartCoroutine(floatingRightT());

    }

    public void startFloatingLeft()
    {

        if (floatFlag)
            StartCoroutine(floatingRightF());
        else if (!floatFlag)
            StartCoroutine(floatingLeftT());

    }

    public void startFloatingForward()
    {

        if (floatFlag)
            StartCoroutine(floatingBackF());
        else if (!floatFlag)
            StartCoroutine(floatingForwardT());

    }

    public void startFloatingBack()
    {

        if (floatFlag)
            StartCoroutine(floatingForwardF());
        else if (!floatFlag)
            StartCoroutine(floatingBackT());

    }



    // below code is for the actual floating of the objects
    IEnumerator floatingUpT()
    {
        gameObject.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + distanceToFloat * Time.deltaTime, transform.localPosition.z);
        yield return new WaitForSeconds(1);
        floatFlag = true;
    }

    IEnumerator floatingUpF()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + distanceToFloat * Time.deltaTime, transform.localPosition.z);
        yield return new WaitForSeconds(1);
        floatFlag = false;
    }

    IEnumerator floatingDownT()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y - distanceToFloat * Time.deltaTime, transform.localPosition.z);
        yield return new WaitForSeconds(1);
        floatFlag = true;
    }

    IEnumerator floatingDownF()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y - distanceToFloat * Time.deltaTime, transform.localPosition.z);
        yield return new WaitForSeconds(1);
        floatFlag = false;
    }

    IEnumerator floatingRightT()
    {
        transform.localPosition = new Vector3(transform.localPosition.x + distanceToFloat * Time.deltaTime, transform.localPosition.y, transform.localPosition.z);
        yield return new WaitForSeconds(1);
        floatFlag = true;
    }

    IEnumerator floatingRightF()
    {
        transform.localPosition = new Vector3(transform.localPosition.x + distanceToFloat * Time.deltaTime, transform.localPosition.y, transform.localPosition.z);
        yield return new WaitForSeconds(1);
        floatFlag = false;
    }

    IEnumerator floatingLeftT()
    {
        transform.localPosition = new Vector3(transform.localPosition.x - distanceToFloat * Time.deltaTime, transform.localPosition.y, transform.localPosition.z);
        yield return new WaitForSeconds(1);
        floatFlag = true;
    }

    IEnumerator floatingLeftF()
    {
        transform.localPosition = new Vector3(transform.localPosition.x - distanceToFloat * Time.deltaTime, transform.localPosition.y, transform.localPosition.z);
        yield return new WaitForSeconds(1);
        floatFlag = false;
    }

    IEnumerator floatingForwardT()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z - distanceToFloat * Time.deltaTime);
        yield return new WaitForSeconds(1);
        floatFlag = true;
    }

    IEnumerator floatingForwardF()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z - distanceToFloat * Time.deltaTime);
        yield return new WaitForSeconds(1);
        floatFlag = false;
    }

    IEnumerator floatingBackT()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z + distanceToFloat * Time.deltaTime);
        yield return new WaitForSeconds(1);
        floatFlag = true;

    }

    IEnumerator floatingBackF()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z + distanceToFloat * Time.deltaTime);
        yield return new WaitForSeconds(1);
        floatFlag = false;

    }


}
