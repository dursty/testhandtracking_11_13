using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

public class DisplayObject {
    public enum ModelType {
        Arrow = 0
    }

    [XmlAttribute("id")]
    public int id;

    [XmlElement(Type = typeof(XmlVector3))]
	public XmlVector3 Translation;

    [XmlAttribute("modelType")]
    public ModelType modelType;

    [XmlAttribute("orientation")]
    public string orientation;

    public Vector3 getTranslateCoord(){
        return Translation.getVector3();
    }

    public string getOrientation(){
        return orientation;
    }

    public ModelType getModel(){
        return modelType;
    }
}
